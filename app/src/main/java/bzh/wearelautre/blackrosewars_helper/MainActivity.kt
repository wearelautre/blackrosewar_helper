package bzh.wearelautre.blackrosewars_helper

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.ListView
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import bzh.wearelautre.blackrosewars_helper.adapter.MagicSchoolAdapter
import bzh.wearelautre.blackrosewars_helper.model.MagicSchool
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity() {

    private lateinit var listView: ListView
    private lateinit var shuffleButton: FloatingActionButton
    private lateinit var switchV2MagicSchool: Switch
    private lateinit var clearButton: Button
    private lateinit var adapter: MagicSchoolAdapter
    private lateinit var magicSchoolList: ArrayList<MagicSchool>
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listView = findViewById(R.id.magic_type_list_view)

        toolbar = findViewById(R.id.topAppBar)
        setSupportActionBar(toolbar)

        loadMagicSchool(false)

        adapter = MagicSchoolAdapter(
            this,
            magicSchoolList
        )
        listView.adapter = adapter

        shuffleButton = findViewById(R.id.floatingActionButton)
        shuffleButton.setOnClickListener {
            this.shuffle()
        }

        clearButton = findViewById(R.id.clear_button)
        clearButton.isEnabled = false
        clearButton.setOnClickListener {
            this.clear()
        }

        switchV2MagicSchool = findViewById(R.id.add_v2_magic_school_switch)
        switchV2MagicSchool.setOnCheckedChangeListener { _, isChecked ->
            loadMagicSchool(isChecked)
            adapter =
                MagicSchoolAdapter(
                    this,
                    magicSchoolList
                )
            listView.adapter = adapter
            clear()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_app_bar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.about -> {
            openAbout()
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun openAbout() {
        val intent = Intent(this, AboutActivity::class.java)
        startActivity(intent)
    }

    private fun loadMagicSchool(withV2MagicSchool: Boolean) {
        magicSchoolList = MagicSchool.getMagicSchoolsFromFile("magicSchools_v1.json", this)
        if (withV2MagicSchool) {
            magicSchoolList.addAll(
                MagicSchool.getMagicSchoolsFromFile(
                    "magicSchools_v2.json",
                    this,
                    magicSchoolList.size
                )
            )
        }
        magicSchoolList.sortBy { magicSchool -> magicSchool.title }

    }

    private fun clear() {
        magicSchoolList.forEach { it.selected = false }
        adapter = MagicSchoolAdapter(
            this,
            magicSchoolList
        )
        listView.adapter = adapter
        clearButton.isEnabled = false
    }

    private fun shuffle() {
        val indexList: MutableList<Int> = listOf(0 until listView.adapter.count)
            .flatten()
            .toMutableList()

        indexList.forEach(action = {
            (listView.adapter.getItem(it) as MagicSchool).selected = false
        })
        val listSelected = emptyList<Int>().toMutableSet()

        for (i in 0..5) {
            val n = (0 until indexList.size).random()
            listSelected.add(indexList[n])
            indexList.removeAt(n)
        }

        val listItem = ArrayList<MagicSchool>()

        magicSchoolList.forEach(action = {
            val item = it
            item.selected = listSelected.contains(it.id)
            listItem.add(item)
        })

        listItem.sortBy { !it.selected }

        adapter = MagicSchoolAdapter(
            this,
            listItem
        )
        listView.adapter = adapter
        adapter.notifyDataSetChanged()
        clearButton.isEnabled = true

    }

}