package bzh.wearelautre.blackrosewars_helper.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import bzh.wearelautre.blackrosewars_helper.R
import bzh.wearelautre.blackrosewars_helper.model.MagicSchool


class MagicSchoolAdapter(
    private val context: Context,
    private val dataSource: ArrayList<MagicSchool>
) : BaseAdapter() {

    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder

        if (convertView == null) {
            view = inflater.inflate(R.layout.list_item_magic_school, parent, false)

            holder = ViewHolder()
            holder.thumbnailImageView =
                view.findViewById(R.id.magic_school_list_thumbnail) as ImageView
            holder.titleTextView = view.findViewById(R.id.magic_school_list_title) as TextView
            holder.waveTextView = view.findViewById(R.id.magic_school_list_wave) as TextView
            holder.extensionTextView = view.findViewById(R.id.magic_school_list_extension) as TextView
            holder.selectedImageView =
                view.findViewById(R.id.magic_school_list_selected) as ImageView

            view.tag = holder
        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        val titleTextView = holder.titleTextView
        val waveTextView = holder.waveTextView
        val extensionTextView = holder.extensionTextView
        val selectedImageView = holder.selectedImageView
        val thumbnailImageView = holder.thumbnailImageView

        titleTextView.typeface = ResourcesCompat.getFont(
            context,
            R.font.cymbeline
        )

        val magicSchool = getItem(position) as MagicSchool

        titleTextView.text = getStringResByString(magicSchool.title)
        waveTextView.text = getStringResByString(magicSchool.vague)
        extensionTextView.text = getStringResByString("ext_${magicSchool.extension}")

        thumbnailImageView.setImageResource(getDrawableResByString(magicSchool.title))
        selectedImageView.setImageResource(R.drawable.selected)
        selectedImageView.isVisible = magicSchool.selected

        return view
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private class ViewHolder {
        lateinit var titleTextView: TextView
        lateinit var waveTextView: TextView
        lateinit var extensionTextView: TextView
        lateinit var thumbnailImageView: ImageView
        lateinit var selectedImageView: ImageView
    }

    private fun getStringResByString(resName: String): String {
        return context.resources.getString(
            getResByString(resName, "string")
        )
    }

    private fun getDrawableResByString(resName: String): Int {
        return getResByString(resName, "drawable")
    }

    private fun getResByString(resName: String, defType: String): Int {
        return context.resources.getIdentifier(
            resName,
            defType,
            "bzh.wearelautre.blackrosewars_helper"
        )
    }
}
