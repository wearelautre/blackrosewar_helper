package bzh.wearelautre.blackrosewars_helper.model

import android.content.Context
import org.json.JSONException
import org.json.JSONObject

class MagicSchool(
    val id: Int,
    val title: String,
    val description: String,
    val extension: String,
    val vague: String,
    var selected: Boolean = false
) {
    companion object {

        fun getMagicSchoolsFromFile(
            filename: String,
            context: Context,
            size: Int = 0
        ): ArrayList<MagicSchool> {
            val magicSchoolList = ArrayList<MagicSchool>()

            try {
                // Load data
                val jsonString =
                    loadJsonFromAsset(
                        filename,
                        context
                    )
                val json = JSONObject(jsonString!!)
                val magicSchools = json.getJSONArray("magicSchools")

                // Get MagicSchool objects from data
                (0 until magicSchools.length()).mapTo(magicSchoolList) {
                    MagicSchool(
                        it + size,
                        magicSchools.getJSONObject(it).getString("title"),
                        magicSchools.getJSONObject(it).getString("description"),
                        magicSchools.getJSONObject(it).getString("extension"),
                        magicSchools.getJSONObject(it).getString("vague")
                    )
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            magicSchoolList.forEach { println(it.vague) }

            return magicSchoolList
        }

        private fun loadJsonFromAsset(filename: String, context: Context): String? {
            val json: String?

            try {
                val inputStream = context.assets.open(filename)
                val size = inputStream.available()
                val buffer = ByteArray(size)
                inputStream.read(buffer)
                inputStream.close()
                json = String(buffer, Charsets.UTF_8)
            } catch (ex: java.io.IOException) {
                ex.printStackTrace()
                return null
            }

            return json
        }
    }
}