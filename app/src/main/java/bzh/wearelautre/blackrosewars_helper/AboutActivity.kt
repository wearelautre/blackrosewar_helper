package bzh.wearelautre.blackrosewars_helper

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity


class AboutActivity : AppCompatActivity() {

    private lateinit var toolbar: androidx.appcompat.widget.Toolbar
    private lateinit var buttonLmsWebsite: Button
    private lateinit var buttonContactMe: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        toolbar = findViewById(R.id.topAppBar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        buttonLmsWebsite = findViewById(R.id.buttonLmsWebsiteLink)
        buttonLmsWebsite.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse("https://ludusmagnusstudio.com")
            startActivity(i)
        }

        buttonContactMe = findViewById(R.id.buttonContactMe)
        buttonContactMe.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_EMAIL, "gestin.gweltaz@gmail.com")
            intent.putExtra(Intent.EXTRA_SUBJECT, "Contact Black Rose Wars Helper")
            intent.type = "message/rfc822"
            startActivity(Intent.createChooser(intent, "Choose Mail App"))
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}